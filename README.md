# README #

Configuration project for git, i3, tmux, vim, zsh

### Restrictions ###

This project only works for Linux and MacOS at the moment, Windows is not
supported.

### What is this repository for? ###

* manage all configuration files for the most important tools

### How do I get set up? ###

This repository is used together with https://gitlab.com/halderm/salt-master.git
to install the configuration.

* install necessary packages
  - install vagrant
  - install virtualbox

```bash
sudo apt-get install vagrant virtualbox
```

* clone this repository

```bash
git clone https://gitlab.com/halderm/salt-master.git
```

* run vagrant

```bash
cd salt-master/masterless
vagrant up
```

* connect to vagrant box

```bash
vagrant ssh
vagrant ssh -- -l halderm
ssh -p 2222 halderm@localhost
```

* use salt inside machine

```bash
sudo salt-call state.highstate
```

* cleanup vagrant box

```bash
vagrant destroy default
vagrant box remove debian/wheezy64
```

### How do I get set up for my current box instead of virtualbox? ###

* install necessary packages
  - install salt-minion, vim, git
  - for MacOS install homebrew

```bash
sudo apt-get install salt-minion
sudo apt-get install vim
sudo apt-get install git
```

* optional: set usecase:server for server use with user buildr

```bash
sudo salt-call grains.setval 'usecase' 'server'
```

* clone this repository and salt-master

```bash
git clone https://gitlab.com/halderm/config.git
git clone https://gitlab.com/halderm/salt-master.git
```

* edit salt minion config

```bash
sudo vim /etc/salt/minion
```

* example patch

```diff
diff --git a/minion b/minion
index 7ce6610..c8841c9 100644
--- a/minion
+++ b/minion
@@ -406 +406 @@
-#file_client: remote
+file_client: local
@@ -423,3 +423,3 @@
-#file_roots:
-#  base:
-#    - /srv/salt
+file_roots:
+  base:
+    - /home/halderm/gitlab/salt-master/masterless/salt/roots/salt
@@ -446,3 +446,3 @@
-#pillar_roots:
-#  base:
-#    - /srv/pillar
+pillar_roots:
+  base:
+    - /home/halderm/gitlab/salt-master/masterless/salt/roots/pillar
```

* restart minion

```bash
sudo service restart salt-minion
```

* adjust users state file

```bash
vim <path_to_salt-master>/masterless/salt/roots/pillar/users.sls
```

* optional: adjust server usecase users file

```bash
vim <path_to_salt-master>/masterless/salt/roots/pillar/users_server.sls
```

* run salt

```bash
sudo salt-call state.highstate
```

### Contributors ###

* Martin Halder
* Dirk Honisch
