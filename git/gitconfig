[user]
    name = Martin Halder
    email = martin.halder@emenda.ch
[diff]
    tool = vimdff
[difftool]
    prompt = false
[merge]
    tool = vimdiff
[color]
    ui = auto
    diff = auto
    status = auto
    branch = auto
[core]
    editor = vim -f
    trustExitCode = true
    excludesfile = ~/.gitignore
[sendemail]
    smtpserver = smtp.gmail.com
    smtpserverport = 587
    smtpencryption = tls
    smtpuser = martin.halder@emenda.ch
[alias]
    st = status
    ci = commit
    co = checkout
    di = diff
    dc = diff --cached
    amend = commit --amend
    aa = add --all
    ff = merge --ff-only
    pullff = pull --ff-only
    noff = merge --no-ff
    fa = fetch --all
    pom = push origin master
    b = branch
    ds = diff --stat=160,120
    dh1 = diff HEAD~1
    gg = log --graph --pretty=oneline --all --abbrev-commit --decorate

    # divergence (commits we added and commits remote added)
    div = divergence

    # goodness (summary of diff lines added/removed/total)
    gn = goodness
    gnc = goodness --cached

    # fancy logging
    # h = head
    # hp = head with patch
    # r = recent commits, only current branch
    # ra = recent commits, all reachable refs
    # l = all commits, only current branch
    # la = all commits, all reachable refs
    head = !git l -1
    h = !git head
    hp = !show-git-head
    r = !git l -30
    ra = !git r --all
    l = !pretty-git-log
    la = !git l --all
    lg = log --graph --pretty=format:'%C(magenta)%h%Creset%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%ae>%Creset' --abbrev-commit --date=relative
    d = difftool
[push]
    default = tracking
[http]
	sslverify = false
[credential]
	helper = cache --timeout 3600
