" set font
set gfn=Ubuntu\ Mono\ 11.00

" highlight current line, could do crosshair but disturbing
set cursorline
set nocursorcolumn

" set guioptions
set go-=m go-=T go-=l go-=L go-=r go-=R go-=b go-=F

" hide the mouse pointer while typing
set mousehide

" set up the gui cursor to look nice
set guicursor=n-v-c:block-Cursor-blinkon0
set guicursor+=ve:ver35-Cursor
set guicursor+=o:hor50-Cursor
set guicursor+=i-ci:ver25-Cursor
set guicursor+=r-cr:hor20-Cursor
set guicursor+=sm:block-Cursor-blinkwait175-blinkoff150-blinkon175

" set the gui options the way I like
set guioptions=ac

" define sign and and mapping
sign define info text=>> linehl=ErrorMsg texthl=ErrorMsg
map ,ss :exe":sign place 1 line=".line(".")." name=info file=".expand("%:p")<CR>
map ,js :exe":sign jump 1 file=".expand("%:p")<CR>
map ,ds :sign unplace<CR>
